package com.foodbites.hotel.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodbites.hotel.model.Menus;


public interface MenuRepository extends JpaRepository<Menus,Integer> {

}
