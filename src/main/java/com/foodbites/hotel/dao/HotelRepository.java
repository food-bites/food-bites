package com.foodbites.hotel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodbites.hotel.model.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, Integer>{

	List<Hotel> getHotelByCity(String city);

	List<Hotel> getHotelByLocation(String location);

	List<Hotel> getHotelByRating(int rating);

	List<Hotel> getHotelByCuisine(String cuisine);  

	
	
}
