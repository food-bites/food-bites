package com.foodbites.hotel.service;

import java.util.List;


@Service
public class HotelServiceImpl implements IHotelService {

	@Autowired
	HotelRepository hotelRepository;

	@Autowired
	MenuRepository menuRepository;

	@Override
	public void addHotel(Hotel hotel) {
		hotelRepository.save(hotel);

	}

	@Override
	public Hotel getHotelById(int hotelId) {
		return hotelRepository.findById(hotelId).get();
	}

	@Override
	public List<Hotel> getHotelByCity(String city) {

		return hotelRepository.getHotelByCity(city);

	}

	@Override
	public List<Hotel> getHotelByLocation(String location) {

		return hotelRepository.getHotelByLocation(location);
	}

	@Override
	public List<Hotel> getHotelByRating(int rating) {

		return hotelRepository.getHotelByRating(rating);
	}

	@Override
	public List<Hotel> getAllHotels() {
		return hotelRepository.findAll();

	}

	@Override
	public void addMenu(Menus menus) {
		menuRepository.save(menus);

	}

	@Override
	public Menus getMenuById(int menuId) {
		return menuRepository.findById(menuId).get();
	}

	@Override
	public List<Hotel> getHotelByCuisine(String cuisine) {

		return hotelRepository.getHotelByCuisine(cuisine);

	}

}
