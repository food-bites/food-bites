package com.foodbites.hotel.service;

import java.util.List;

import com.foodbites.hotel.model.Hotel;
import com.foodbites.hotel.model.Menus;

public interface IHotelService {

	public void addHotel(Hotel hotel);

	public Hotel getHotelById(int hotelId);

	List<Hotel> getHotelByCity(String city);

	List<Hotel> getHotelByLocation(String location);

	List<Hotel> getHotelByRating(int rating);
	
	List<Hotel> getHotelByCuisine(String cuisine);

	List<Hotel> getAllHotels();

	public void addMenu(Menus menus);

	public Menus getMenuById(int menuId);

}
