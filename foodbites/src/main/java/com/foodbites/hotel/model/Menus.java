package com.foodbites.hotel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column; 

@Entity
public class Menus {
	@Id
	@GeneratedValue(generator = "menusseq")
	@GenericGenerator(name = "menusseq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "menus_sequence"),
			@Parameter(name = "initial_value", value = "201"), @Parameter(name = "increment_size", value = "1") })
	private int itemId;
	
	@Column(length=10)
	private String itemName;
	
	@Column(length=5)
	private float price;
	
	@Column(length=6)
	private String type;

	@ManyToOne
	@JoinColumn(name="hotel_Id")
	@JsonBackReference
	private Hotel hotel;
	
	
	public Menus(int itemId, String itemName, float price, String type, Hotel hotel) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.price = price;
		this.type = type;
		this.hotel = hotel;
	}
	
	public Menus() {
		super();
		
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	

	@Override
	public String toString() {
		return "Menus [itemId=" + itemId + ", itemName=" + itemName + ", price=" + price + ", type=" + type + ", hotel="
				+ hotel + "]";
	}

	
	
	
	
}
