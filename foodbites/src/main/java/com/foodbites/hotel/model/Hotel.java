package com.foodbites.hotel.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;

@Entity
public class Hotel {
	@Id
	@GeneratedValue(generator = "hotelseq")
	@GenericGenerator(name = "hotelseq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "hotel_sequence"),
			@Parameter(name = "initial_value", value = "101"), @Parameter(name = "increment_size", value = "1") })

	private int hotelId;

	@Column(length = 10)
	private String hotelName;

	@Column(length = 10)
	private String city;

	@Column(length = 10)
	private String location;

	@Column(length = 10)
	private String cuisine;

	private int rating;

	@Column(length = 10)
	private String image;

	@OneToMany(mappedBy = "hotel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Menus> menuList;

	
	public Hotel(int hotelId, String hotelName, String city, String location, String cuisine, int rating, String image,
			List<Menus> menuList) {
		super();
		this.hotelId = hotelId;
		this.hotelName = hotelName;
		this.city = city;
		this.location = location;
		this.cuisine = cuisine;
		this.rating = rating;
		this.image = image;
		this.menuList = menuList;
	}
	

	public Hotel() {
		super();

	}

	public int getHotelId() {
		return hotelId;
	}

	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<Menus> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menus> menuList) {
		this.menuList = menuList;
	}

	@Override
	public String toString() {
		return "Hotel [hotelId=" + hotelId + ", hotelName=" + hotelName + ", city=" + city + ", location=" + location
				+ ", cuisine=" + cuisine + ", rating=" + rating + ", image=" + image + ", menuList=" + menuList + "]";
	}

}
